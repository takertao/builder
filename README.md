# builder
Docker-compose config used to run a Teller node.

### Explanation of Services

#### Web2
JSON-RPC API server. Provides methods for getting loan terms, user data, and calculating risk.

#### Postgres

#### Redis

#### Contracts Events API
API to get info from contracts events DB.

#### Contracts Events Listener
Listens for GraphQL events from The Graph. Enqueues events onto a message queue
using redis.
https://thegraph.com/explorer/subgraph/salazarguille/zero-collateral

#### Contracts Events Worker
Dequeues events from the Redis message queue and adds them to the DB.
